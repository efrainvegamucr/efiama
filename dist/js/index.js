const app = Vue.createApp({
    data() {
        return {
            API: "077ba60dfab14d7f9e0c2ce858ddef8d",
            user: [
                {
                    image: "./images/profile.png",
                    username: "Efrain Alberto Vega Morua",
                    email: "efrainvegam27@gmail.com"
                }
            ],
            categories: [
                { name: "main course" },
                { name: "side dish" },
                { name: "dessert" },
                { name: "appetizer" },
                { name: "salad" },
                { name: "bread" },
                { name: "breakfast" },
                { name: "soup" },
                { name: "beverage" },
                { name: "sauce" },
                { name: "marinade" },
                { name: "fingerfood" },
                { name: "snack" },
                { name: "drink" }
            ],
            favorites: [],
            top_recipes: [],
            recipes: [],
            recipe: {}
        }
    },
    mounted: function () {
        //localStorage.clear();
        this.favorites = JSON.parse(localStorage.getItem('favorites'));

        if (window.location.pathname === '/efiama/dist/index.html') {
            //Connect to API to get a Top Recipes list
            axios({
                method: 'get',
                url: 'https://api.spoonacular.com/recipes/complexSearch?&sort=popularity&number=5&apiKey=' + this.API,
            })
            .then(
                (response) => {
                    let items = response.data.results;

                    this.top_recipes = [];

                    items.forEach(element => {
                        this.top_recipes.push({
                            id: element.id,
                            name: element.title,
                            image: element.image
                        })
                    });

                    for (let i = 0; i < this.top_recipes.length; i++) {
                        //Connect to API to get a Detail of Recipe
                        this.addDetailsRecipe(this.top_recipes[i]);
                    }
                }
            )
            .catch(
                error => console.log(error)
            );
            
            //Connect to API to get a Default Recipes list
            axios({
                method: 'get',
                url: 'https://api.spoonacular.com/recipes/complexSearch?&apiKey=' + this.API,
            })
            .then(
                (response) => {
                    let items = response.data.results;

                    this.recipes = [];

                    items.forEach(element => {
                        this.recipes.push({
                            id: element.id,
                            name: element.title,
                            image: element.image,
                        })
                    });

                    for (let i = 0; i < this.recipes.length; i++) {
                        //Connect to API to get a Detail of Recipe
                        this.addDetailsRecipe(this.recipes[i]);
                    }
                }
            )
            .catch(
                error => console.log(error)
            );
        }
    },
    methods: {
        onClickSelectedCategory(category) {
            axios({
                method: 'get',
                url: 'https://api.spoonacular.com/recipes/complexSearch?type=' + category + '&apiKey=' + this.API,
            })
            .then(
                (response) => {
                    let items = response.data.results;

                    this.recipes = [];

                    items.forEach(element => {
                        this.recipes.push({
                            id: element.id,
                            name: element.title,
                            image: element.image,
                        })
                    });

                    for (let i = 0; i < this.recipes.length; i++) {
                        //Connect to API to get a Detail of Recipe
                        axios({
                            method: 'get',
                            url: 'https://api.spoonacular.com/recipes/' + this.recipes[i].id + '/information?includeNutrition=false&apiKey=' + this.API,
                        })
                        .then(
                            (response) => {
                                let item = response.data;

                                this.recipes[i]['category'] = item.dishTypes[0];
                                this.recipes[i]['total'] = item.readyInMinutes + " mins",
                                    this.recipes[i]['level'] = "Easy",
                                    this.recipes[i]['likes'] = item.aggregateLikes
                            }
                        )
                        .catch(
                            error => console.log(error)
                        );
                    }
                }
            )
            .catch(
                error => console.log(error)
            );
        },
        onClickViewRecipe(index) {
            axios({
                method: 'get',
                url: 'https://api.spoonacular.com/recipes/' + index + '/information?includeNutrition=false&apiKey=' + this.API,
            })
            .then(
                (response) => {
                    let item = response.data;

                    this.recipe.id = index;
                    this.recipe.name = item.title;
                    this.recipe.image = item.image;
                    this.recipe.description = this.removeTags(item.summary);

                    this.recipe.preparation = item.preparationMinutes + " mins";
                    this.recipe.cooking = item.cookingMinutes + " mins";
                    this.recipe.total = item.readyInMinutes + " mins";

                    this.recipe.level = "Easy";
                    this.recipe.category = item.dishTypes[0];
                    this.recipe.occasion = item.occasions[0];


                    this.recipe.portions = item.servings;
                    this.recipe.likes = item.aggregateLikes;

                    this.recipe.instructions = this.removeTags(item.instructions);

                    let ingredientsList = "";
                    for (let i = 0; i < item.extendedIngredients.length; i++) {
                        ingredientsList += "- " + item.extendedIngredients[i].original + "\n"
                    }

                    this.recipe.ingredients = ingredientsList;
                }
            )
            .catch(
                error => console.log(error)
            );
        },
        onClickLikeRecipe(index) {
            axios({
                method: 'get',
                url: 'https://api.spoonacular.com/recipes/' + index + '/information?includeNutrition=false&apiKey=' + this.API,
            })
            .then(
                (response) => {
                    let item = response.data;
                    let exist = false;

                    if (this.favorites === null) {
                        this.favorites = [];
                    }

                    for (let i = 0; i < this.favorites.length; i++) {
                        if (this.favorites[i].id == index) {
                            exist = true;
                        }
                    }
                    if (!exist) {
                        this.favorites.push({
                            id: index,
                            name: item.title,
                            image: item.image,
                            category: item.dishTypes[0],
                            total: item.readyInMinutes + " mins",
                            level: "Easy",
                            likes: item.aggregateLikes
                        })
                        console.log("La receta ha sido agregada.")
                    }
                    else {
                        console.log("La receta ya está agregada.")
                    }
                    localStorage.setItem('favorites', JSON.stringify(this.favorites));
                }
            )
            .catch(
                error => console.log(error)
            );
        },
        addDetailsRecipe(recipe) {
            //Connect to API to get a Detail of Recipe
            axios({
                method: 'get',
                url: 'https://api.spoonacular.com/recipes/' + recipe.id + '/information?includeNutrition=false&apiKey=' + this.API,
            })
            .then(
                (response) => {
                    let item = response.data;

                    recipe['category'] = item.dishTypes[0];
                    recipe['total'] = item.readyInMinutes + " mins",
                        recipe['level'] = "Easy",
                        recipe['likes'] = item.aggregateLikes
                }
            )
            .catch(
                error => console.log(error)
            );
        },
        searchRecipe() {
            axios({
                method: 'get',
                url: 'https://api.spoonacular.com/recipes/complexSearch?query=' + this.$refs.searchInput.value + '&apiKey=' + this.API
            })
            .then(response => {
                let items = response.data.results;

                this.recipes = [];

                items.forEach(element => {
                    this.recipes.push({
                        id: element.id,
                        name: element.title,
                        image: element.image,
                    })
                });

                for (let i = 0; i < this.recipes.length; i++) {
                    //Connect to API to get a Detail of Recipe
                    axios({
                        method: 'get',
                        url: 'https://api.spoonacular.com/recipes/' + this.recipes[i].id + '/information?includeNutrition=false&apiKey=' + this.API,
                    })
                        .then(
                            (response) => {
                                let item = response.data;

                                this.recipes[i]['category'] = item.dishTypes[0];
                                this.recipes[i]['total'] = item.readyInMinutes + " mins",
                                    this.recipes[i]['level'] = "Easy",
                                    this.recipes[i]['likes'] = item.aggregateLikes
                            }
                        )
                        .catch(
                            error => console.log(error)
                        );
                }
            }
            )
            .catch(
                error => console.log(error)
            );
        },
        removeTags(str) {
            if ((str === null) || (str === ''))
                return false;
            else
                str = str.toString();
            return str.replace(/(<([^>]+)>)/ig, '');
        }
    }
})