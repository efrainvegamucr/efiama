app.component('recipe-category-button', {
    props:{
        name:{
            type: String
        }
    },
    data() {
        return {
            counter: 0
        }
    },
    methods:{
        onClickCategoryButton(){
            this.$emit('selectedcategory', this.name);
        }
    },
    template: 
    /*html*/
    `
    <button class="btn btn-danger custome-btn" v-on:click="onClickCategoryButton">{{ name }}</button>
    `
})