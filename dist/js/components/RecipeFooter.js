app.component('recipe-footer', {
    props: {
    },
    data() {
        return {
            informacion: [
                "Contact Us",
                "Terms & Conditions",
                "Privacy Policy",
                "Accesibility Statement"
            ],
            copyright: "2023 © EFIAMA - Todos los derechos reservados"
        }

    },
    methods: {
    },
    template:
        /*html*/
        `
        <footer class="py-4 background-footer">
            <div class="container">
                <ul class="nav justify-content-center footer-text-link my-2">
                    <li v-for="(item, index) in informacion" class="nav-item mx-2 mb-2">
                        <button class="nav-link nav-text" href="#"> {{ item }} </button>
                    </li>
                </ul>
                <div class="container px-4 px-lg-5 my-2">
                    <div class="text-center footer-text"> {{ copyright }} </div>
                </div>
            </div>
        </footer>

        `
})