app.component('recipe-detail', {
    props: {
        name: {
            type: String,
            default: "default name"
        },
        image: {
            type: String,
        },
        preparation: {
            type: String,
            default: "default preparation"
        },
        cooking: {
            type: String,
            default: "default cooking"
        },
        total: {
            type: String,
            default: "default total"
        },
        portions: {
            type: Number,
            default: 0
        },
        level: {
            type: String,
            default: "default level"
        },
        category: {
            type: String,
            default: "default category"
        },
        occasion: {
            type: String,
            default: "default occasion"
        },
        likes: {
            type: Number,
            default: 0
        },
        description: {
            type: String,
            default: "default description"
        },
        ingredients: {
            type: String,
            default: "default ingredients"
        },
        instructions: {
            type: String,
            default: "default instructions"
        }
    },
    mounted() {

    },
    methods: {
    },
    template:
    /*html*/
    `
    <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
            aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title mx-auto card-detail-title" id="staticBackdropLabel">{{ name }}</h1>
                    <button type="button" class="btn-close" style="margin: 0" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <img class="d-block m-auto img-fluid" v-bind:src="image" alt="{{ name }}">

                    <div class="container text-center">
                        <p class="my-4 card-detail-description">{{ description }}</p>
                            <div class="row my-2 border border-danger row-cols-1 row-cols-sm-3">
                                <div class="col my-3">
                                    <p class="card-detail-subtitle"> Preparation Time </p>
                                    <p class="card-detail-item"> {{ preparation }}</p>
                                </div>
                                <div class="col my-3">
                                    <p class="card-detail-subtitle"> Cooking Time </p>
                                    <p class="card-detail-item"> {{ cooking }}</p>
                                </div>
                                <div class="col my-3">
                                    <p class="card-detail-subtitle"> Total Time </p>
                                    <p class="card-detail-item"> {{ total }} </p>
                                </div>
                            </div>

                            <div class="row my-2 border border-danger row-cols-1 row-cols-sm-3">
                                <div class="col my-3 ">
                                    <p class="card-detail-subtitle"> Complexity </p>
                                    <p class="card-detail-item">{{ level }}</p>
                                </div>
                                <div class="col my-3">
                                    <p class="card-detail-subtitle"> Category </p>
                                    <p class="card-detail-item">{{ category }}</p>
                                </div>
                                <div class="col my-3">
                                    <p class="card-detail-subtitle"> Ocassion </p>
                                    <p class="card-detail-item">{{ occasion }}</p>
                                </div>
                            </div>

                            <div class="row my-2 border border-danger">
                                <div class="col my-3">
                                    <p class="card-detail-subtitle"> Number of Servings </p>
                                    <p class="card-detail-item">{{ portions }}</p>
                                </div>
                            </div>

                            <div class="my-4">
                                <img src="././images/heart.svg" class="card-img-top rounded" alt="featured recipe" width="32" height="32">
                                <p class="">{{ likes }}</p>
                            </div>
                    </div>
                    <div class="container text-start">
                        <h3 class="mt-4 mb-2 text-center"> Ingredients </h3>
                        <div class="row my-2 border border-danger">
                            <p class="card-detail-text">{{ ingredients }}</p>
                        </div>
                        <h3 class="mt-4 mb-2 text-center"> Instructions </h3>
                        <div class="row my-3 border border-danger">
                            <p class="card-detail-text">{{ instructions }}</p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    `
})