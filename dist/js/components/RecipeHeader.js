app.component('recipe-header', {
    props: {
    },
    data() {
        return {
            logo: "EFIAMA",
            navegation: [
                { etiqueta: "Inicio", referencia: "index.html" },
                { etiqueta: "Top Recetas", referencia: "index.html#recipe-top" },
                { etiqueta: "Mis Recetas", referencia: "profile.html" },
                { etiqueta: "Iniciar Sesión", referencia: "login.html" }
            ]
        }

    },
    methods: {
    },
    template:
        /*html*/
        `
        <header class="navbar navbar-expand-lg navbar-light py-3 background-header px-5" id="recipe-home">
            <div class="container">
                <a class="navbar-brand logo-header" href="index.html#app"> {{ logo }} </a>

                <button class="navbar-toggler navbar-toggler-right navbar-dark" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                    aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>

                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ms-auto my-2 my-lg-0 align-items-center">
                        <li v-for="(item, index) in navegation" class="nav-item mx-3">
                            <a v-bind:href="item.referencia" class="nav-link nav-text text-center" aria-current="page">{{ item.etiqueta }}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
        `
})