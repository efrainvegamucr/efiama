app.component('recipe-card', {
    props: {
        image: {
            type: String
        },
        category: {
            type: String,
            default: "NA"
        },
        name: {
            type: String,
            default: "default name"
        },
        description: {
            type: String,
            default: "default description"
        },
        total: {
            type: String,
            default: "0"
        },
        level: {
            type: String,
            default: "NA"
        },
        likes: {
            type: Number,
            default: 0
        },
        index: {
            type: Number
        }
    },
    data() {
        return {
            recipe_likes: 0
        }
    },
    mounted() {
        this.recipe_likes = this.likes;
    },
    methods: {
        onClickViewRecipe() {
            this.$emit('recipedetail', this.index);
        },
        onClickLikeRecipe() {
            this.$emit('recipelike', this.index);
        }
    },
    template:
        /*html*/
        `
        <div class="card bg-light p-3 h-100 w-100">
            <img v-bind:src="image" class="card-img-top rounded" alt="featured recipe">
            <div class="d-flex flex-column card-body p-0">
                <h5 class="row card-filter-name h-100 align-items-center justify-content-center text-center mt-4">
                    {{ name }}</h5>
                <p class="card-filter-category my-3">{{ category }}</p>
                <div class="mt-auto my-3">
                    <p class="mb-0 card-filter-time-level">{{ total }}</p>
                    <p class="mb-0 card-filter-time-level">{{ level }}</p>
                </div>
                <div class="my-2">
                    <button type="submit" class="btn btn-primary-outline" v-on:click="onClickLikeRecipe">
                        <img src="././images/heart.svg" class="card-img-top rounded" alt="featured recipe" width="32" height="32">
                    </button>
                    <a class="mb-0 card-filter-time-level mb-3">{{ likes }}</a>
                </div>
                
                <button href="#" class="btn btn-danger mt-auto mx-auto justify-content-center w-100"
                    v-on:click="onClickViewRecipe" data-bs-toggle="modal" data-bs-target="#staticBackdrop">View Recipe</button>
            </div>
        </div>
        `
})